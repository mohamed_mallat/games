import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Game } from '../interfaces/game.interface';

@Injectable({
  providedIn: 'root'
})
export class HomeService {
  constructor() { }

  games: Game[] = [
    {
      title: 'League of Legends',
      image: 'https://static-cdn.jtvnw.net/ttv-boxart/21779-285x380.jpg',
      score: 9,
      releasedDate: '1255739462',
      description: "League of Legends, commonly referred to as League, is a 2009 multiplayer online battle arena video game developed and published by Riot Games. Inspired by Defense of the Ancients, a custom map for Warcraft III, Riot's founders sought to develop a stand-alone game in the same genre"
    },
    {
      title: 'League of Legends',
      image: 'https://static-cdn.jtvnw.net/ttv-boxart/21779-285x380.jpg',
      score: 9,
      releasedDate: '1255739462',
      description: "League of Legends, commonly referred to as League, is a 2009 multiplayer online battle arena video game developed and published by Riot Games. Inspired by Defense of the Ancients, a custom map for Warcraft III, Riot's founders sought to develop a stand-alone game in the same genre"
    },
    {
      title: 'League of Legends',
      image: 'https://static-cdn.jtvnw.net/ttv-boxart/21779-285x380.jpg',
      score: 9,
      releasedDate: '1255739462',
      description: "League of Legends, commonly referred to as League, is a 2009 multiplayer online battle arena video game developed and published by Riot Games. Inspired by Defense of the Ancients, a custom map for Warcraft III, Riot's founders sought to develop a stand-alone game in the same genre"
    },
    {
      title: 'League of Legends',
      image: 'https://static-cdn.jtvnw.net/ttv-boxart/21779-285x380.jpg',
      score: 9,
      releasedDate: '1255739462',
      description: "League of Legends, commonly referred to as League, is a 2009 multiplayer online battle arena video game developed and published by Riot Games. Inspired by Defense of the Ancients, a custom map for Warcraft III, Riot's founders sought to develop a stand-alone game in the same genre"
    },
    {
      title: 'League of Legends',
      image: 'https://static-cdn.jtvnw.net/ttv-boxart/21779-285x380.jpg',
      score: 9,
      releasedDate: '1255739462',
      description: "League of Legends, commonly referred to as League, is a 2009 multiplayer online battle arena video game developed and published by Riot Games. Inspired by Defense of the Ancients, a custom map for Warcraft III, Riot's founders sought to develop a stand-alone game in the same genre"
    },
    {
      title: 'League of Legends',
      image: 'https://static-cdn.jtvnw.net/ttv-boxart/21779-285x380.jpg',
      score: 9,
      releasedDate: '1255739462',
      description: "League of Legends, commonly referred to as League, is a 2009 multiplayer online battle arena video game developed and published by Riot Games. Inspired by Defense of the Ancients, a custom map for Warcraft III, Riot's founders sought to develop a stand-alone game in the same genre"
    },
    {
      title: 'League of Legends',
      image: 'https://static-cdn.jtvnw.net/ttv-boxart/21779-285x380.jpg',
      score: 9,
      releasedDate: '1255739462',
      description: "League of Legends, commonly referred to as League, is a 2009 multiplayer online battle arena video game developed and published by Riot Games. Inspired by Defense of the Ancients, a custom map for Warcraft III, Riot's founders sought to develop a stand-alone game in the same genre"
    },
    {
      title: 'League of Legends',
      image: 'https://static-cdn.jtvnw.net/ttv-boxart/21779-285x380.jpg',
      score: 9,
      releasedDate: '1255739462',
      description: "League of Legends, commonly referred to as League, is a 2009 multiplayer online battle arena video game developed and published by Riot Games. Inspired by Defense of the Ancients, a custom map for Warcraft III, Riot's founders sought to develop a stand-alone game in the same genre"
    }
  ]

  public getGames(): Observable<Game[]> {
    return of(this.games);
  }
}
