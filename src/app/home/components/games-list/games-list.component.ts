import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Game } from '../../interfaces/game.interface';
import { HomeService } from '../../services/home.service';

@Component({
  selector: 'app-games-list',
  templateUrl: './games-list.component.html',
  styleUrls: ['./games-list.component.scss']
})
export class GamesListComponent implements OnInit {

  public games$: Observable<Game[]> | undefined; // TODO Change any to the specific type

  constructor(private readonly homeService: HomeService) { }

  ngOnInit(): void {
    this.getGames()
  }

  private getGames(): void {
    this.games$ = this.homeService.getGames();
  }
  

}
