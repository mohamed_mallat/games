export interface Game {
    title: string;
    releasedDate: string;
    description: string;
    score: number;
    image: string;
}